/*
Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
*/

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => {
	console.log(data)
})


/*
Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
*/
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then ((data) => console.log(data))


/*
Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
*/

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		id: 123,
		title: "S33-Activity",
		userId: 6
	})
})
.then((response) => response.json())
.then((data) => console.log(data))


/*
Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
*/

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated to do list item'
	})
})
.then((response) => response.json())
.then((data) => console.log(data))


/*
Update a to do list item by changing the data structure to contain the following properties:
a. Title
b. Description
c. Status
d. Date Completed
e. User ID
*/

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'To do list',
		description: 'Updated data structure',
		status: 'Completed',
		dateCompleted: '10-03-22',
		userId: '123'

	})
})
.then((response) => response.json())
.then((data) => console.log(data))


/*
Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
*/

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated to do list item'
	})
})
.then((response) => response.json())
.then((data) => console.log(data))


/*
Update a to do list item by changing the status to complete and add a date when the status was changed.
*/


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		completed: 'true',
		dateCompleted: '10-03-22'

	})
})
.then((response) => response.json())
.then((data) => console.log(data))



/*
Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
*/

fetch('https://jsonplaceholder.typicode.com/todos/2', {
	method: 'DELETE'
})










